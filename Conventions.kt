// 1) "Comparison"

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override operator fun compareTo(other: MyDate): Int {
        return when {
            year != other.year -> year - other.year
            month != other.month -> month - other.month
            else -> dayOfMonth - other.dayOfMonth
        }
    }
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}

// 2) "Ranges"

fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in first..last
}

// 3) "For loop"

class DateRange(val start: MyDate, val end: MyDate) : Iterable<MyDate> {
    override operator fun iterator(): Iterator<MyDate> {
        return object : Iterator<MyDate> {
            var date: MyDate = start
            override operator fun hasNext(): Boolean {
                return date <= end
            }
            override operator fun next(): MyDate {
                var next = date
                date = date.followingDate()
                return next
            }
        }
    }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}

// 4) "Operators overloading"

import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

// Supported intervals that might be added to dates:
enum class TimeInterval { DAY, WEEK, YEAR }

operator fun TimeInterval.times(amount: Int): MultipleTimeIntervals = MultipleTimeIntervals(this, amount)

data class MultipleTimeIntervals(val timeInterval: TimeInterval, val amount: Int)

operator fun MyDate.plus(timeInterval: TimeInterval): MyDate = this.addTimeIntervals(timeInterval, 1)

operator fun MyDate.plus(multipleTimeIntervals: MultipleTimeIntervals): MyDate
        = this.addTimeIntervals(multipleTimeIntervals.timeInterval, multipleTimeIntervals.amount)

fun task1(today: MyDate): MyDate {
    return today + YEAR + WEEK
}

fun task2(today: MyDate): MyDate {
    return today + YEAR * 2 + WEEK * 3 + DAY * 5
}

// 5) "Invoke"

class Invokable {
    var numberOfInvocations: Int = 0
        private set

    operator fun invoke(): Invokable {
        this.numberOfInvocations++
        return this
    }
}

fun invokeTwice(invokable: Invokable) = invokable()()
